# Logger

A console and file(s) log library.
Automatic zipping and deletion of old logs is supported.
Logging runs on a separate thread.

File logger supports changes to the path on-the-fly as well as buffering logs before the path is actually set (handy in case logging should already be done before the actual path is known and/or available).

Logger has 5 severity levels, each level has a positive and a negative message:

| Severity | Negative | Positive    |
| -------- | -------- | ----------- |
| Fatal    | Fatality | Win         |
| Major    | Error    | Success     |
| Minor    | Warning  | Information |
| Debug    | Caution  | Debug       |
| Trace    | Notice   | Trace       |

## Usage

Create the loggers:

```csharp
var loggerFile = new LoggerFile("File", "./default.log");
var loggerConsole = new LoggerConsole("Console");
var logger = new Logger(loggerFile, loggerConsole);
```

Settings for the console logger:

```csharp
loggerConsole.Level = LoggerSeverity.MAJOR;
```

Settings for the file logger:

```csharp
loggerFile.Severity = LoggerSeverity.MINOR;
loggerFile.ZipOldLogs = true;
loggerFile.MaximumLogsInTextFile = 5;
loggerFile.MaximumLogsInZipFile = 1000;
loggerFile.MaximumSizeOfZipFile = 1048576 /*1MB */;
loggerFile.Level = LoggerSeverity.MINOR;
```

Now you can use it:

```csharp
logger.Win("The purpose of the application is now fulfilled.");
logger.Success("Success! Something was done and done all right.");
logger.Information("General log information.");
logger.Debug("Debugging info, like x = 10.");
logger.Trace("Code was here.");
logger.Notice("A notice that is not even worth a caution.");
logger.Caution("A low level warning, just be cautious.");
logger.Warning("A serious warning happened.");
logger.Error("A recoverable error encountered.", GenerateException());
logger.Fatality("A fatal error encountered!",    GenerateException());
```

## Example

As of now, only outputs to the console and to a file are supported.
In case you want to use your own writer an `OnLog` event is presented.

### Console

Console output looks like this:

![Log output to consolog](/Examples/Simple/Screenshot.Console.2.png)

Colors are customizable.

### File
Log file looks like this:

```
─────────────────────────┐
2020/11/23 19:50:46.822  |
2020/11/23 19:50:46.704  |  WIN:         The purpose of the application is now fulfilled.
2020/11/23 19:50:46.713  |  SUCCESS:     Success! Something was done and done all right.
2020/11/23 19:50:46.713  |  INFORMATION: General log information.
2020/11/23 19:50:46.713  |  DEBUG:       Debugging info, like x = 10.
2020/11/23 19:50:46.714  |  TRACE:       Code was here.
2020/11/23 19:50:46.714  |  NOTICE:      A notice that is not even worth a caution.
2020/11/23 19:50:46.714  |  CAUTION:     A low level warning, just be cautious.
2020/11/23 19:50:46.714  |  WARNING:     A serious warning happened.
2020/11/23 19:50:46.748  |  ERROR:       A recoverable error encountered.
2020/11/23 19:50:46.748  |  ERROR:       
2020/11/23 19:50:46.748  |  ERROR:       1. Original exception
2020/11/23 19:50:46.748  |  ERROR:       2. Inner rethrown exception
2020/11/23 19:50:46.748  |  ERROR:       3. Intermediate rethrown exception
2020/11/23 19:50:46.748  |  ERROR:       4. Outer rethrown exception
2020/11/23 19:50:46.748  |  ERROR:       
2020/11/23 19:50:46.748  |  ERROR:       Call stack trace:
2020/11/23 19:50:46.748  |  ERROR:          at Iodynis.Libraries.Logging.Example.Program.GenerateInnerException() in D:\libraries\C#\.NET Framework\1st\Logger\Examples\Simple\Program.cs:line 65
2020/11/23 19:50:46.748  |  ERROR:       
2020/11/23 19:50:46.807  |  FATALITY:    A fatal error encountered!
2020/11/23 19:50:46.807  |  FATALITY:    
2020/11/23 19:50:46.807  |  FATALITY:    1. Original exception
2020/11/23 19:50:46.807  |  FATALITY:    2. Inner rethrown exception
2020/11/23 19:50:46.807  |  FATALITY:    3. Intermediate rethrown exception
2020/11/23 19:50:46.807  |  FATALITY:    4. Outer rethrown exception
2020/11/23 19:50:46.807  |  FATALITY:    
2020/11/23 19:50:46.807  |  FATALITY:    Call stack trace:
2020/11/23 19:50:46.807  |  FATALITY:       at Iodynis.Libraries.Logging.Example.Program.GenerateInnerException() in D:\libraries\C#\.NET Framework\1st\Logger\Examples\Simple\Program.cs:line 65
2020/11/23 19:50:46.807  |  FATALITY:    
 __    _  _         _    /
/  \  / \/ \_    /\/ \  /
    \/       \  /     \/
              \/
 
     (`/\
     `=\/\
      `=\/\
       `=\/
          \_
          ) (
         (___)

─────────────────────────┐
2020/11/23 19:50:49.270  |
2020/11/23 19:50:49.160  |  WIN:         The purpose of the application is now fulfilled.
2020/11/23 19:50:49.168  |  SUCCESS:     Success! Something was done and done all right.
2020/11/23 19:50:49.169  |  INFORMATION: General log information.
2020/11/23 19:50:49.169  |  DEBUG:       Debugging info, like x = 10.
2020/11/23 19:50:49.169  |  TRACE:       Code was here.
2020/11/23 19:50:49.169  |  NOTICE:      A notice that is not even worth a caution.
2020/11/23 19:50:49.169  |  CAUTION:     A low level warning, just be cautious.
2020/11/23 19:50:49.170  |  WARNING:     A serious warning happened.
2020/11/23 19:50:49.204  |  ERROR:       A recoverable error encountered.
2020/11/23 19:50:49.204  |  ERROR:       
2020/11/23 19:50:49.204  |  ERROR:       1. Original exception
2020/11/23 19:50:49.204  |  ERROR:       2. Inner rethrown exception
2020/11/23 19:50:49.204  |  ERROR:       3. Intermediate rethrown exception
2020/11/23 19:50:49.204  |  ERROR:       4. Outer rethrown exception
2020/11/23 19:50:49.204  |  ERROR:       
2020/11/23 19:50:49.204  |  ERROR:       Call stack trace:
2020/11/23 19:50:49.204  |  ERROR:          at Iodynis.Libraries.Logging.Example.Program.GenerateInnerException() in D:\libraries\C#\.NET Framework\1st\Logger\Examples\Simple\Program.cs:line 65
2020/11/23 19:50:49.204  |  ERROR:       
2020/11/23 19:50:49.242  |  FATALITY:    A fatal error encountered!
2020/11/23 19:50:49.242  |  FATALITY:    
2020/11/23 19:50:49.242  |  FATALITY:    1. Original exception
2020/11/23 19:50:49.242  |  FATALITY:    2. Inner rethrown exception
2020/11/23 19:50:49.242  |  FATALITY:    3. Intermediate rethrown exception
2020/11/23 19:50:49.242  |  FATALITY:    4. Outer rethrown exception
2020/11/23 19:50:49.242  |  FATALITY:    
2020/11/23 19:50:49.242  |  FATALITY:    Call stack trace:
2020/11/23 19:50:49.242  |  FATALITY:       at Iodynis.Libraries.Logging.Example.Program.GenerateInnerException() in D:\libraries\C#\.NET Framework\1st\Logger\Examples\Simple\Program.cs:line 65
2020/11/23 19:50:49.242  |  FATALITY:    
2020/11/23 19:50:49.771  |
─────────────────────────┘
───────────────────┘  |
──────────────────────┘
```

In case of an abrupt termination the next time the program runs the log file is appended with a torn paper sheet ASCII-art.
In case of a normal termination the log is appended with an undamaged paper sheets ASCII-art.
These ASCII-art prefixes and postfixes can be customized or turned off.

## License

Library is available under the MIT license.

Repository icon is from https://ikonate.com/ pack and is used under the MIT license.