﻿using System;

namespace Iodynis.Libraries.Logging.Example
{
    static class Program
    {
        public static void Main(params string[] arguments)
        {
            // Setup the output to file
            var loggerOutputFile = new LoggerOutputFile("File", "./default.log");
            loggerOutputFile.SeverityThreshold = LoggerSeverity.TRACE;
            loggerOutputFile.ZipOldLogs = true;
            loggerOutputFile.MaximumLogsInTextFile = 5;
            loggerOutputFile.MaximumLogsInZipFile = 1000;
            loggerOutputFile.MaximumSizeOfZipFile = 1048576 /*1MB */;
            loggerOutputFile.Format.LogChannel = true;
            loggerOutputFile.Format.LogType = true;
            loggerOutputFile.Format.LogTimeStamp = true;
            loggerOutputFile.Format.LogThread = true;
            loggerOutputFile.Format.OmitSameType = true;
            loggerOutputFile.Format.OmitSameChannel = true;
            loggerOutputFile.Format.OmitSameThread = true;
            loggerOutputFile.Format.OmitMainThread = false;

            // Setup the output to console
            var loggerOutputConsole = new LoggerOutputConsole("Console");
            loggerOutputConsole.SeverityThreshold = LoggerSeverity.MINOR;
            loggerOutputConsole.Format.TypeFatality = "OOOPSIE:     ";
            loggerOutputConsole.Format.LogChannel = false;
            loggerOutputConsole.Format.LogType = false;
            loggerOutputConsole.Format.LogTimeStamp = false;
            loggerOutputConsole.Format.LogThread = false;

            // Configure different channels. Each channel can have different settings.
            var loggerChannelFile = new LoggerChannel("file", loggerOutputFile);
            var loggerChannelConsole = new LoggerChannel("console", loggerOutputConsole);
            var loggerChannelDefault = new LoggerChannel("default", loggerOutputFile, loggerOutputConsole);

            var loggerManager = new LoggerManager();
            loggerManager.AddChannels(loggerChannelFile, loggerChannelConsole, loggerChannelDefault);

            // Channels
            loggerChannelFile.Information("This is printed to the file only.");
            loggerChannelConsole.Information("This is printed to the console only.");
            loggerChannelDefault.Information("This is printed both to the file and to the console.");
            loggerChannelDefault.Trace("This is printed to the file only. It is not printed to the console due to the console severity threshold.");

            // Append
            loggerChannelDefault.NewLine();
            loggerChannelDefault.Error("You can append to an already logged line.");
            loggerChannelDefault.Append("Append over a default delimiter (space)");
            loggerChannelDefault.Append("or append over a custom delimiter (comma).", ", ");

            // Conditions
            loggerChannelDefault.NewLine();
            loggerChannelDefault.If(false).Information("If condition is false. This line is not logged, other line is.").Else().Information("Else condition is true. This line is logged, the other line is not.");
            loggerChannelDefault.If(true).Information("If condition is true. This line is logged, the other line is not.").Else().Information("Else condition is false. This line is not logged, other line is.");
            loggerChannelDefault.Information("Before condition").If(true).Information("In if.").Else().Information("In else.").Uncondition().Information("After condition.");
            loggerChannelDefault.Information("Before condition").If(false).Information("In if.").Else().Information("In else.").Uncondition().Information("After condition.");

            // Chaining
            loggerChannelDefault.NewLine();
            loggerChannelDefault.Information("Chain").Information("some").Information("lines.");

            // Scopes
            loggerChannelDefault.NewLine();
            loggerChannelDefault.Information("Scope:");
            loggerChannelDefault.ScopeStart();
            loggerChannelDefault.Information("Indented once.");
            loggerChannelDefault.ScopeStart();
            loggerChannelDefault.Information("Indented twice.");
            loggerChannelDefault.ScopeEnd();
            loggerChannelDefault.ScopeEnd();

            // Scopes
            loggerChannelDefault.NewLine();
            using (loggerChannelDefault.Information("Scope 1 header.").Scope())
            {
                loggerChannelDefault.Information("Inside scope 1.");
                using (loggerChannelDefault.Information("Scope 2 header.").Scope())
                {
                    loggerChannelDefault.Information("Inside scope 2.");
                }
            }

            // Objects
            loggerChannelDefault.NewLine();
            loggerChannelDefault.Information("Log some structs and objects:");
            loggerChannelDefault.Object(DateTime.Now, LoggerType.NONE);
            loggerChannelDefault.Append(loggerChannelDefault, ", ");

            // Custom printers

            // Exceptions
            loggerChannelDefault.NewLine();
            loggerChannelDefault.Caution(GenerateException());
            loggerChannelDefault.Warning("Custom message to print before exception.", GenerateException());

            // Severity and types
            loggerChannelDefault.NewLine();
            loggerChannelDefault.Win("The purpose of the application is now fulfilled.");
            loggerChannelDefault.Success("Success! Something was done and done all right.");
            loggerChannelDefault.Information("General log information.");
            loggerChannelDefault.Debug("Debugging info, like x = 10.");
            loggerChannelDefault.Trace("Code was here.");

            loggerChannelDefault.NewLine();
            loggerChannelDefault.Notice("A notice that is not even worth a caution.");
            loggerChannelDefault.Caution("A low level warning, just be cautious.");
            loggerChannelDefault.Warning("A serious warning happened.");
            loggerChannelDefault.Error("A recoverable error encountered.", GenerateException());
            loggerChannelDefault.Fatality("A fatal error encountered!",    GenerateException());

            Console.ReadKey();
        }
        private static Exception GenerateException()
        {
            try
            {
                GenerateOuterException();
            }
            catch (Exception exception)
            {
                return exception;
            }
            return null;
        }
        private static void GenerateOuterException()
        {
            try
            {
                GenerateIntermediateException();
            }
            catch (Exception exception)
            {
                throw new Exception("Outer rethrown exception", exception);
            }
        }
        private static void GenerateIntermediateException()
        {
            try
            {
                GenerateInnerException();
            }
            catch (Exception exception)
            {
                throw new Exception("Intermediate rethrown exception", exception);
            }
        }
        private static void GenerateInnerException()
        {
            try
            {
                throw new Exception("Original exception");
            }
            catch (Exception exception)
            {
                throw new Exception("Inner rethrown exception", exception);
            }
        }
    }
}
