﻿using System;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Log level
    /// </summary>
    public enum LoggerSeverity : int
    {
        /// <summary>
        /// Log nothing.
        /// </summary>
        NONE = 0,

        /// <summary>
        /// Log everything (<see cref="LoggerType.FATALITY"/>, <see cref="LoggerType.ERROR"/>, <see cref="LoggerType.WARNING"/>, <see cref="LoggerType.CAUTION"/>, <see cref="LoggerType.NOTICE"/>, <see cref="LoggerType.WIN"/>, <see cref="LoggerType.SUCCESS"/>, <see cref="LoggerType.INFORMATION"/>, <see cref="LoggerType.DEBUG"/>, <see cref="LoggerType.TRACE"/>).
        /// </summary>
        TRACE = 1,
        /// <summary>
        /// Log everything excluding trace (<see cref="LoggerType.FATALITY"/>, <see cref="LoggerType.ERROR"/>, <see cref="LoggerType.WARNING"/>, <see cref="LoggerType.CAUTION"/>, <see cref="LoggerType.WIN"/>, <see cref="LoggerType.SUCCESS"/>, <see cref="LoggerType.INFORMATION"/>, <see cref="LoggerType.DEBUG"/>).
        /// </summary>
        DEBUG = 2,
        /// <summary>
        /// Log all errors and warnings (<see cref="LoggerType.FATALITY"/>, <see cref="LoggerType.ERROR"/>, <see cref="LoggerType.WARNING"/>, <see cref="LoggerType.WIN"/>, <see cref="LoggerType.SUCCESS"/>, <see cref="LoggerType.INFORMATION"/>).
        /// </summary>
        MINOR = 3,
        /// <summary>
        /// Log all errors (<see cref="LoggerType.FATALITY"/>, <see cref="LoggerType.ERROR"/>, <see cref="LoggerType.WIN"/>, <see cref="LoggerType.SUCCESS"/>).
        /// </summary>
        MAJOR = 4,
        /// <summary>
        /// Log only fatal errors (<see cref="LoggerType.FATALITY"/>, <see cref="LoggerType.WIN"/>).
        /// </summary>
        EXCEPTIONAL = 5,
    }
}
