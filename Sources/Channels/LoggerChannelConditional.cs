﻿using System;

namespace Iodynis.Libraries.Logging
{
    public struct LoggerChannelConditional
    {
        private LoggerChannel LoggerChannel;
        private bool Condition;

        public LoggerChannelConditional(LoggerChannel loggerChannel)
            : this(loggerChannel, true) { }
        public LoggerChannelConditional(LoggerChannel loggerChannel, bool condition)
        {
            LoggerChannel = loggerChannel;
            Condition = condition;
        }

        public LoggerChannelConditional If(bool condition)
        {
            Condition = condition;
            return this;
        }
        public LoggerChannelConditional Else()
        {
            Condition = !Condition;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public LoggerChannel Uncondition()
        {
            return LoggerChannel;
        }

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.NewLine()"/>
        /// </summary>
        /// <returns><inheritdoc cref="LoggerChannel.NewLine()" path="/returns"/></returns>
        public LoggerChannelConditional Empty()
        {
            if (Condition)
            {
                LoggerChannel.NewLine();
            }

            return this;
        }

        #region Debug

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Debug(string)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Debug(string)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Debug(string)" path="/returns"/></returns>
        public LoggerChannelConditional Debug(string text = "")
        {
            if (Condition)
            {
                LoggerChannel.Debug(text);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Debug(Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Debug(Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Debug(Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Debug(Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Debug(exception);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Debug(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Debug(string, Exception)" path="/param[1]"/></param>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Debug(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Debug(string, Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Debug(string text, Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Debug(text, exception);
            }

            return this;
        }

        #endregion

        #region Information

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Information(string)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Information(string)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Information(string)" path="/returns"/></returns>
        public LoggerChannelConditional Information(string text = "")
        {
            if (Condition)
            {
                LoggerChannel.Information(text);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Information(Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Information(Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Information(Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Information(Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Information(exception);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Information(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Information(string, Exception)" path="/param[1]"/></param>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Information(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Information(string, Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Information(string text, Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Information(text, exception);
            }

            return this;
        }

        #endregion

        #region Notice

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Notice(string)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Notice(string)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Notice(string)" path="/returns"/></returns>
        public LoggerChannelConditional Notice(string text = "")
        {
            if (Condition)
            {
                LoggerChannel.Notice(text);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Notice(Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Notice(Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Notice(Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Notice(Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Notice(exception);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Notice(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Notice(string, Exception)" path="/param[1]"/></param>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Notice(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Notice(string, Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Notice(string text, Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Notice(text, exception);
            }

            return this;
        }

        #endregion

        #region Caution

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Caution(string)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Caution(string)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Caution(string)" path="/returns"/></returns>
        public LoggerChannelConditional Caution(string text = "")
        {
            if (Condition)
            {
                LoggerChannel.Caution(text);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Caution(Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Caution(Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Caution(Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Caution(Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Caution(exception);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Caution(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Caution(string, Exception)" path="/param[1]"/></param>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Caution(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Caution(string, Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Caution(string text, Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Caution(text, exception);
            }

            return this;
        }

        #endregion

        #region Warning

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Warning(string)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Warning(string)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Warning(string)" path="/returns"/></returns>
        public LoggerChannelConditional Warning(string text = "")
        {
            if (Condition)
            {
                LoggerChannel.Warning(text);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Warning(Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Warning(Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Warning(Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Warning(Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Warning(exception);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Warning(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Warning(string, Exception)" path="/param[1]"/></param>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Warning(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Warning(string, Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Warning(string text, Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Warning(text, exception);
            }

            return this;
        }

        #endregion

        #region Error

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Error(string)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Error(string)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Error(string)" path="/returns"/></returns>
        public LoggerChannelConditional Error(string text = "")
        {
            if (Condition)
            {
                LoggerChannel.Error(text);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Error(Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Error(Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Error(Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Error(Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Error(exception);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Error(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Error(string, Exception)" path="/param[1]"/></param>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Error(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Error(string, Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Error(string text, Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Error(text, exception);
            }

            return this;
        }

        #endregion

        #region Fatality

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Fatality(string)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Fatality(string)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Fatality(string)" path="/returns"/></returns>
        public LoggerChannelConditional Fatality(string text = "")
        {
            if (Condition)
            {
                LoggerChannel.Fatality(text);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Fatality(Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Fatality(Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Fatality(Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Fatality(Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Fatality(exception);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Fatality(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Fatality(string, Exception)" path="/param[1]"/></param>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Fatality(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Fatality(string, Exception)" path="/returns"/></returns>
        public LoggerChannelConditional Fatality(string text, Exception exception)
        {
            if (Condition)
            {
                LoggerChannel.Fatality(text, exception);
            }

            return this;
        }

        #endregion

        #region Success

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Success(string)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Success(string)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Success(string)" path="/returns"/></returns>
        public LoggerChannelConditional Success(string text = "")
        {
            if (Condition)
            {
                LoggerChannel.Success(text);
            }

            return this;
        }

        #endregion

        #region Win

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Win(string)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Win(string)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Win(string)" path="/returns"/></returns>
        public LoggerChannelConditional Win(string text = "")
        {
            if (Condition)
            {
                LoggerChannel.Win(text);
            }

            return this;
        }

        #endregion

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Object{T}(T, LoggerType)"/>
        /// </summary>
        /// <param name="object"><inheritdoc cref="LoggerChannel.Object{T}(T, LoggerType)" path="/param[2]"/></param>
        /// <param name="type"><inheritdoc cref="LoggerChannel.Object{T}(T, LoggerType)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Object{T}(T, LoggerType)" path="/returns"/></returns>
        public LoggerChannelConditional Object<T>(T @object, LoggerType type = LoggerType.NONE)
        {
            if (Condition)
            {
                LoggerChannel.Object(@object, type);
            }

            return this;
        }

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Log(string, LoggerType)"/>
        /// </summary>
        /// <param name="type"><inheritdoc cref="LoggerChannel.Log(string, LoggerType)" path="/param[1]"/></param>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Log(string, LoggerType)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannelConditional Log(string text, LoggerType type = LoggerType.NONE)
        {
            if (Condition)
            {
                LoggerChannel.Log(text, type);
            }

            return this;
        }
    }
}
