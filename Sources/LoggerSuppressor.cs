﻿using System;
using System.Threading;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Defines a scope, inside which the corresponding logging is disabled.
    /// </summary>
    public class LoggerSuppressor : IDisposable
    {
        private int Counter = 0;
        internal bool IsInScope => Counter > 0;

        internal LoggerSuppressor() { }

        internal LoggerSuppressor ScopeStart()
        {
            Interlocked.Increment(ref Counter);
            return this;
        }
        internal LoggerSuppressor ScopeStop()
        {
            Interlocked.Decrement(ref Counter);
            return this;
        }
        /// <summary>
        /// Close the scope.
        /// Should only be called automatically on exit from a using statement.
        /// </summary>
        public void Dispose()
        {
            ScopeStop();
        }
    }
}
