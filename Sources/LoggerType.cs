﻿using System;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Log level
    /// </summary>
    public enum LoggerType : int
    {
        /// <summary>
        /// The final goal of the application is accomplished.
        /// </summary>
        WIN = 5,
        /// <summary>
        /// An operation completed successfully.
        /// </summary>
        SUCCESS = 4,
        /// <summary>
        /// General information. Usually about high-level operations normal execution flow.
        /// </summary>
        INFORMATION = 3,
        /// <summary>
        /// Debug information. Usually about internal state and values.
        /// </summary>
        DEBUG = 2,
        /// <summary>
        /// Verbose information. Usually about step-wise details of the execution flow.
        /// </summary>
        TRACE = 1,

        /// <summary>
        /// Raw data. Will be printed as is (no prefix, new line or other modifications).
        /// </summary>
        NONE = 0,

        /// <summary>
        /// Verbose information. Usually about something that is technically totally OK, but at the same time unusual and unexpected.
        /// </summary>
        NOTICE = -1,
        /// <summary>
        /// An information to pay attention to. This time the situation is OK, but may lead to problems in the future.
        /// </summary>
        CAUTION = -2,
        /// <summary>
        /// A warning. A workaround for this situation is known. No data is lost and the execution flow is unaffected.
        /// </summary>
        WARNING = -3,
        /// <summary>
        /// A recoverable error. Some data may be lost, but the application can continue running with some flow changes.
        /// </summary>
        ERROR = -4,
        /// <summary>
        /// A critical error. The application cannot continue and has to terminate.
        /// </summary>
        FATALITY = -5,
    }

    internal static partial class Extensions
    {
        /// <summary>
        /// Convert a <see cref="LoggerType"/> to a <see cref="LoggerSeverity"/>.
        /// </summary>
        /// <param name="type">The <see cref="LoggerType"/> to convert.</param>
        /// <returns>The corresponding <see cref="LoggerSeverity"/>.</returns>
        /// <exception cref="Exception">Thrown when the <see cref="LoggerType"/> is not supported.</exception>
        public static LoggerSeverity ToSeverity(this LoggerType type)
        {
            //int typeValue = (int)type;
            //typeValue = typeValue > 0 ? typeValue : -typeValue;
            //return (LoggerSeverity)typeValue;

            switch (type)
            {
                case LoggerType.NONE:
                    return LoggerSeverity.NONE;
                case LoggerType.TRACE:
                case LoggerType.NOTICE:
                    return LoggerSeverity.TRACE;
                case LoggerType.DEBUG:
                case LoggerType.CAUTION:
                    return LoggerSeverity.DEBUG;
                case LoggerType.INFORMATION:
                case LoggerType.WARNING:
                    return LoggerSeverity.MINOR;
                case LoggerType.SUCCESS:
                case LoggerType.ERROR:
                    return LoggerSeverity.MAJOR;
                case LoggerType.WIN:
                case LoggerType.FATALITY:
                    return LoggerSeverity.EXCEPTIONAL;
                default:
                    throw new Exception($"Logger type {type} is not supported.");
            }
        }
    }
}
