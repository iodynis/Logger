﻿using System;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Base format settings for the plain text output of the logger.
    /// </summary>
    public class LoggerOutputTextFormat
    {
        /// <summary>
        /// String to use as a new line sequence (default is \r\n for Windows and \n for Linux/Unix/MacOS).
        /// </summary>
        public string NewLine = Environment.NewLine;

        /// <summary>
        /// Indentation string to use for the nested scopes.
        /// </summary>
        public string ScopeIndentation = "    ";

        /// <summary>
        /// Delimiter to use when appending an already logged line.
        /// </summary>
        public string AppendDelimiter = " ";

        #region Timestamp

        /// <summary>
        /// Should log the time stamps (like 2020/12/12 23:52:35.931) or not.
        /// </summary>
        public bool LogTimeStamp;
        /// <summary>
        /// Time stamp format to use.
        /// </summary>
        private string _TimeStampFormat = "yyyy/MM/dd HH:mm:ss.ffff";
        /// <summary>
        /// <inheritdoc cref="_TimeStampFormat"/>
        /// </summary>
        public string TimeStampFormat
        {
            get
            {
                return _TimeStampFormat;
            }
            set
            {
                _TimeStampFormat = value;
                _TimeStampDefault = null;
            }
        }
        /// <summary>
        /// Default string to print for empty timestamp.
        /// </summary>
        private string _TimeStampDefault;
        /// <summary>
        /// <inheritdoc cref="_TimeStampDefault"/>
        /// </summary>
        public string TimeStampDefault => _TimeStampDefault ?? (_TimeStampDefault = DateTime.Now.ToString(TimeStampFormat).Length.Times(" "));

        /// <summary>
        /// Do not print the timestamp if it is the same as the previous log.
        /// </summary>
        public bool OmitSameTimestamp = true;

        #endregion

        #region Channel

        /// <summary>
        /// Should log the channel name or not.
        /// </summary>
        public bool LogChannel;
        /// <summary>
        /// Number of characters to resurve for the channel name.
        /// If the channel name is longer -- it will be truncated if the <see cref="ChannelLengthForce"/> flag is set.
        /// </summary>
        private int _ChannelLength = 10;
        /// <summary>
        /// <inheritdoc cref="_ChannelLength"/>
        /// </summary>
        public int ChannelLength
        {
            get
            {
                return _ChannelLength;
            }
            set
            {
                _ChannelLength = value;
                _ChannelDefault = null;
            }
        }
        /// <summary>
        /// If the channel name is longer than <see cref="ChannelLength"/> it will be truncated to that length.
        /// </summary>
        public bool ChannelLengthForce = false;

        /// <summary>
        /// Default string to print for default/missing channel name.
        /// </summary>
        private string _ChannelDefault;
        /// <summary>
        /// <inheritdoc cref="_ChannelDefault"/>
        /// </summary>
        public string ChannelDefault => _ChannelDefault ?? (_ChannelDefault = _ChannelLength.Times(" "));
        /// <summary>
        /// Do not print the channel name if it is the same as the previous log.
        /// </summary>
        public bool OmitSameChannel = false;

        #endregion

        #region Thread

        /// <summary>
        /// Should log the thread ID or not.
        /// </summary>
        public bool LogThread { get; set; }
        /// <summary>
        /// Do not print the ID of thread for the main thread logs.
        /// </summary>
        public bool OmitMainThread = true;
        /// <summary>
        /// Number of characters to reserve for the thread ID.
        /// If the thread ID is longer -- it will be truncated if the <see cref="ChannelLengthForce"/> flag is set.
        /// </summary>
        private int _ThreadLength = 8;
        /// <summary>
        /// <inheritdoc cref="_ThreadLength"/>
        /// </summary>
        public int ThreadLength
        {
            get
            {
                return _ThreadLength;
            }
            set
            {
                _ThreadLength = value;
                _ThreadDefault = null;
            }
        }
        /// <summary>
        /// If the thread name is longer than <see cref="ThreadLength"/> it will be truncated to that length.
        /// </summary>
        public bool ThreadLengthForce = false;

        /// <summary>
        /// Default string to print for main/missing thread name.
        /// </summary>
        public string _ThreadDefault;
        /// <summary>
        /// <inheritdoc cref="_ThreadDefault"/>
        /// </summary>
        public string ThreadDefault => _ThreadDefault ?? (_ThreadDefault = ThreadLength.Times(" "));
        /// <summary>
        /// Do not print the trhead ID if it is the same as the previous log.
        /// </summary>
        public bool OmitSameThread = false;

        #endregion

        #region Type

        /// <summary>
        /// Should log the type of the event (like DEBUG, WARNING, ERROR, etc.) or not.
        /// </summary>
        public bool LogType { get; set; }
        /// <summary>
        /// Empty prefix.
        /// </summary>
        public string _TypeEmpty = "             ";
        /// <summary>
        /// <inheritdoc cref="_TypeEmpty"/>
        /// </summary>
        public string TypeEmpty
        {
            get
            {
                return _TypeEmpty;
            }
            set
            {
                _TypeEmpty = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Trace prefix.
        /// </summary>
        public string _TypeTrace = "TRACE:       ";
        /// <summary>
        /// <inheritdoc cref="_TypeTrace"/>
        /// </summary>
        public string TypeTrace
        {
            get
            {
                return _TypeTrace;
            }
            set
            {
                _TypeTrace = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Debug prefix.
        /// </summary>
        public string _TypeDebug = "DEBUG:       ";
        /// <summary>
        /// <inheritdoc cref="_TypeDebug"/>
        /// </summary>
        public string TypeDebug
        {
            get
            {
                return _TypeDebug;
            }
            set
            {
                _TypeDebug = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Main information prefix.
        /// </summary>
        public string _TypeInformation = "INFORMATION: ";
        /// <summary>
        /// <inheritdoc cref="_TypeInformation"/>
        /// </summary>
        public string TypeInformation
        {
            get
            {
                return _TypeInformation;
            }
            set
            {
                _TypeInformation = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Notice prefix.
        /// </summary>
        public string _TypeNotice = "NOTICE:      ";
        /// <summary>
        /// <inheritdoc cref="_TypeNotice"/>
        /// </summary>
        public string TypeNotice
        {
            get
            {
                return _TypeNotice;
            }
            set
            {
                _TypeNotice = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Caution prefix.
        /// </summary>
        public string _TypeCaution = "CAUTION:     ";
        /// <summary>
        /// <inheritdoc cref="_TypeCaution"/>
        /// </summary>
        public string TypeCaution
        {
            get
            {
                return _TypeCaution;
            }
            set
            {
                _TypeDebug = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Warning prefix.
        /// </summary>
        public string TypeWarning = "WARNING:     ";
        /// <summary>
        /// <inheritdoc cref="_TypeWarning"/>
        /// </summary>
        public string _TypeWarning
        {
            get
            {
                return _TypeWarning;
            }
            set
            {
                _TypeWarning = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Recoverable error prefix.
        /// </summary>
        public string _TypeError = "ERROR:       ";
        /// <summary>
        /// <inheritdoc cref="_TypeError"/>
        /// </summary>
        public string TypeError
        {
            get
            {
                return _TypeError;
            }
            set
            {
                _TypeError = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Fatal unrecoverable error prefix.
        /// </summary>
        public string _TypeFatality = "FATALITY:    ";
        /// <summary>
        /// <inheritdoc cref="_TypeFatality"/>
        /// </summary>
        public string TypeFatality
        {
            get
            {
                return _TypeFatality;
            }
            set
            {
                _TypeFatality = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Small success prefix.
        /// </summary>
        public string _TypeSuccess = "SUCCESS:     ";
        /// <summary>
        /// <inheritdoc cref="_TypeSuccess"/>
        /// </summary>
        public string TypeSuccess
        {
            get
            {
                return _TypeSuccess;
            }
            set
            {
                _TypeSuccess = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Huge success prefix.
        /// </summary>
        public string _TypeWin = "WIN:         ";
        /// <summary>
        /// <inheritdoc cref="_TypeWin"/>
        /// </summary>
        public string TypeWin
        {
            get
            {
                return _TypeWin;
            }
            set
            {
                _TypeWin = value;
                NormalizeSeverityLengths();

            }
        }
        /// <summary>
        /// Do not print the type name if it is the same as the previous log.
        /// </summary>
        public bool OmitSameType = false;

        private void NormalizeSeverityLengths()
        {
            int Length = 0;
            Length = Math.Max(Length, TypeEmpty.Length);
            Length = Math.Max(Length, TypeTrace.Length);
            Length = Math.Max(Length, TypeDebug.Length);
            Length = Math.Max(Length, TypeInformation.Length);
            Length = Math.Max(Length, TypeSuccess.Length);
            Length = Math.Max(Length, TypeWin.Length);
            Length = Math.Max(Length, TypeNotice.Length);
            Length = Math.Max(Length, TypeCaution.Length);
            Length = Math.Max(Length, TypeWarning.Length);
            Length = Math.Max(Length, TypeError.Length);
            Length = Math.Max(Length, TypeFatality.Length);

            while (TypeEmpty.Length < Length) TypeEmpty += " ";
            while (TypeTrace.Length < Length) TypeTrace += " ";
            while (TypeDebug.Length < Length) TypeDebug += " ";
            while (TypeInformation.Length < Length) TypeInformation += " ";
            while (TypeSuccess.Length < Length) TypeSuccess += " ";
            while (TypeWin.Length < Length) TypeWin += " ";
            while (TypeNotice.Length < Length) TypeNotice += " ";
            while (TypeCaution.Length < Length) TypeCaution += " ";
            while (TypeWarning.Length < Length) TypeWarning += " ";
            while (TypeError.Length < Length) TypeError += " ";
            while (TypeFatality.Length < Length) TypeFatality += " ";
        }

        #endregion
    }
}
