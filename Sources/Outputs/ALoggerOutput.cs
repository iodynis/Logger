﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Abstract logger output class.
    /// </summary>
    public abstract class ALoggerOutput
    {
        /// <summary>
        /// Fires when internal exceptions occur.
        /// </summary>
        public event EventHandler<Exception> OnException;

        /// <summary>
        /// Collection of channels that use this logger output.
        /// </summary>
        private readonly HashSet<LoggerChannel> Channels = new HashSet<LoggerChannel>();
        /// <summary>
        /// Is logger output already initialized.
        /// </summary>
        protected internal bool IsInitialized { get; private set; } = false;

        /// <summary>
        /// Threshold severity level to log to.
        /// </summary>
        public LoggerSeverity SeverityThreshold = LoggerSeverity.TRACE;

        /// <summary>
        /// Thread IDs to allow logs from.
        /// </summary>
        public readonly List<int> ThreadsAllowed = new List<int>();

        /// <summary>
        /// Thread IDs to deny logs from.
        /// </summary>
        public readonly List<int> ThreadsForbidden = new List<int>();

        /// <summary>
        /// Name is used for convenience only.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// If should perform the logging.
        /// </summary>
        public bool IsEnabled => !_DisableScope.IsInScope;
        private LoggerSuppressor _DisableScope = new LoggerSuppressor();
        /// <summary>
        /// For use in using statements only.
        /// </summary>
        public LoggerSuppressor DisableScope => _DisableScope.ScopeStart();

        /// <summary>
        /// Instantiate a logger output with the specified name.
        /// </summary>
        /// <param name="name">The name of the logger output.</param>
        protected ALoggerOutput(string name)
        {
            Name = name;
        }

        internal void Initialize(LoggerChannel channel)
        {
            if (Channels.Contains(channel))
            {
                throw new LoggerInitializationException($"Logger channel {channel} is already using the {this} logger output.");
            }

            Channels.Add(channel);

            if (!IsInitialized)
            {
                Initialize();
            }
        }
        internal void Initialize()
        {
            if (IsInitialized)
            {
                throw new LoggerInitializationException($"Logger output {Name} is already initialized.");
            }

            InitializeInternal();

            IsInitialized = true;
        }
        internal void Deinitialize(LoggerChannel channel)
        {
            Channels.Remove(channel);

            if (IsInitialized && Channels.Count == 0)
            {
                Deinitialize();
            }
        }
        internal void Deinitialize()
        {
            if (IsInitialized)
            {
                throw new LoggerInitializationException($"Logger output {Name} is not yet initialized.");
            }

            DeinitializeInternal();

            IsInitialized = false;
        }
        /// <summary>
        /// Custom initialization logic.
        /// </summary>
        protected virtual void InitializeInternal()
        {
            ;
        }
        /// <summary>
        /// Custom deinitialization logic.
        /// </summary>
        protected virtual void DeinitializeInternal()
        {
            ;
        }
        /// <summary>
        /// Log text.
        /// </summary>
        /// <param name="text">Text to log.</param>
        /// <param name="type">Type.</param>
        /// <param name="time">Time stamp.</param>
        public void Log(string text, LoggerType type, DateTime time)
        {
            Log(null, text, type, time, 0);
        }
        /// <summary>
        /// Log text.
        /// </summary>
        /// <param name="channel">Channel to log.</param>
        /// <param name="text">Text to log.</param>
        /// <param name="type">Type.</param>
        /// <param name="time">Time stamp.</param>
        /// <param name="indentation">Indentation depth.</param>
        public virtual void Log(string channel, string text, LoggerType type, DateTime time, int indentation)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Append to the last logged line.
        /// Please consider not using this in multithreaded environment, as the appending will be done to the last logged line
        /// (which may be not the one you've just logged on your thread, but some other one logged from another thread).
        /// </summary>
        /// <param name="text">Text to append.</param>
        /// <param name="delimiter">Delimiter to append before the text.</param>
        public virtual void Append(string text, string delimiter = null)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Invoke <see cref="OnException"/> event.
        /// </summary>
        /// <param name="exception">The exception to invoke the <see cref="OnException"/> event with.</param>
        protected void InvokeOnException(Exception exception)
        {
            OnException?.Invoke(this, exception);
        }
    }
}
