﻿using System;
using System.Collections.Concurrent;

namespace Iodynis.Libraries.Logging
{
    internal class Pool<T>
    {
        private static ConcurrentBag<T> Items = new ConcurrentBag<T>();
        private Func<T> Create;
        private Action<T> Reset;
        internal Pool(Func<T> create, Action<T> reset = null)
        {
            Create = create;
            Reset = reset;
        }
        internal T Allocate()
        {
            if (Items.TryTake(out T item))
            {
                Reset?.Invoke(item);
                return item;
            }
            return Create();
        }
        //internal PoolDisposable<T> Disposable()
        //{
        //    return new PoolDisposable<T>(this, Allocate());
        //}
        internal void Deallocate(T item)
        {
            Items.Add(item);
        }
    }
}
