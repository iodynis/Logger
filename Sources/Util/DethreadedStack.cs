﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Iodynis.Libraries.Logging
{
    internal class DethreadedStack<T> : IEnumerable<T>, ICollection, IEnumerable
    {
        private static Dictionary<int, Stack<T>> ThreadIdToStack = new Dictionary<int, Stack<T>>();

        public int Count => Stack.Count;

        public object SyncRoot => Stack;

        public bool IsSynchronized => true;

        internal DethreadedStack()
        {
            ;
        }
        private Stack<T> Stack
        {
            get
            {
                if (!ThreadIdToStack.TryGetValue(Environment.CurrentManagedThreadId, out Stack<T> stack))
                {
                    stack = new Stack<T>();
                    ThreadIdToStack.Add(Environment.CurrentManagedThreadId, stack);
                }
                return stack;
            }
        }

        internal void Push(T item)
        {
            // Push the item to the stack
            Stack.Push(item);
        }
        internal T Peek()
        {
            // Get the stack
            if (!ThreadIdToStack.TryGetValue(Environment.CurrentManagedThreadId, out Stack<T> stack))
            {
                return default;
            }
            // Check the stack
            if (stack.Count == 0)
            {
                return default;
            }
            // Peek the top item from the stack
            return stack.Peek();
        }
        internal T Pop()
        {
            // Get the stack
            if (!ThreadIdToStack.TryGetValue(Environment.CurrentManagedThreadId, out Stack<T> stack))
            {
                return default;
            }
            // Check the stack
            if (stack.Count == 0)
            {
                return default;
            }
            // Pop the top item from the stack
            return stack.Pop();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Stack.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Stack.GetEnumerator();
        }

        public void CopyTo(Array array, int index)
        {
            Stack.CopyTo((T[])array, index);
        }
    }
}
