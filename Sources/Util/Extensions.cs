﻿using System.Text;

namespace Iodynis.Libraries.Logging
{
    internal static partial class Extensions
    {
        /// <summary>
        /// Repeat specified string specified number of times.
        /// </summary>
        /// <param name="count">Times to repeat.</param>
        /// <param name="string">String to repeat.</param>
        /// <returns>Repeated string.</returns>
        public static string Times(this int count, string @string)
        {
            if (count <= 0)
            {
                return "";
            }
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < count; i++)
            {
                stringBuilder.Append(@string);
            }
            return stringBuilder.ToString();
        }
    }
}