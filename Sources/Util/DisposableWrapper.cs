﻿//using System;

//namespace Iodynis.Libraries.Logging
//{
//    internal class DisposableWrapper<T> : IDisposable
//    {
//        private T Item;
//        private Action<T> Action;

//        public DisposableWrapper(T item, Action<T> action)
//        {
//            Item = item;
//            Action = action;
//        }

//        public void Dispose()
//        {
//            Action(Item);
//        }

//        public static implicit operator T (DisposableWrapper<T> wrapper)
//        {
//            return wrapper.Item;
//        }
//    }
//}
