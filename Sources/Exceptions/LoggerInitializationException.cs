﻿using System;

namespace Iodynis.Libraries.Logging
{
    [Serializable]
    public class LoggerInitializationException : Exception
    {
        public LoggerInitializationException() : base() { }
        public LoggerInitializationException(string message) : base(message) { }
        public LoggerInitializationException(string message, Exception innerException) : base(message, innerException) { }
    }
}
