﻿namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Time formatting style.
    /// </summary>
    public enum LoggerTimeFormat
    {
        /// <summary>
        /// No formatting, just ticks.
        /// </summary>
        NONE = 0,

        /// <summary>
        /// Short format, like: 1h 4m, 4m 5s, 8.12s, 9.777ms
        /// </summary>
        SHORT = 1,

        /// <summary>
        /// Long format, like: 1 hour and 4 minutes, 4 minutes and 5 seconds, 8 seconds and 12 milliseconds, 9 milliseconds and 777 nanoseconds
        /// Short format, like: 1h 4m, 4m 5s, 8.12s, 9.777ms
        /// </summary>
        LONG = 2,
    }
}
